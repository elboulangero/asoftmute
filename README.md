`asoftmute` was written for audio cards that don't have pswitch capabilities,
ie cards that can't be muted/unmuted natively with ALSA.

`asoftmute` just emulates *mute* by setting the volume to zero,
and *unmute* by restoring the previous volume.

It requires the program `amixer` to work.
